<?php

$result = [
    "from_trail" => [
      "3afcdbfeb6ecfbdd0ba628696e3cc163" =>[
        "0" => [
          'id'        => $nid,
          'bundle'    => $bundle->name,
          'callback'  => 'node_load'
        ],
        "1" => [
          'id'        => $taxonomy_term->tid,
          'bundle'    => $bundle->machine_name,
          'callback'  => 'taxonomy_term_load'
        ],
        "2" => [
          'id'        => $view->name,
          'bundle'    => $view->name,
          'args'      => $view->argument,
          'callback'  => 'views_get_view'
        ],
      ],
    ],
];
