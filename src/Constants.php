<?php

namespace Drupal\follow_me;

/**
 * Module constants.
 * Copied from old D7 version. Might need to find another spot for these.
 */

abstract class Constants {

  /**
   *
   */
  const FOME_TRAIL_QS_VAR = 'from_trail';


  /**
   *
   */
  const FOME_CACHE_TRAIL = 'follow_me';

}
