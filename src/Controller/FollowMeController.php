<?php

/**
 * Follow Me controller.
 */

namespace Drupal\follow_me\Controller;

use Drupal\Core\Controller\ControllerBase;

class FollowMeController extends ControllerBase {

  public function content() {

    return [
      '#theme' => 'follow_me_previous',
      '#previous_link' => $this->t('Test link text')
    ];

  }

}