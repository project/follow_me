<?php

/**
 * Custom class for building components.
 * NOT IN USE YET.
 */

namespace Drupal\follow_me;

class ComponentBuilder {

  protected $component;

  public function __construct($component) {

  }

  /**
   * Build tail component for nodes keyed by content type.
   *
   * @return array
   */
  private function buildNodeComponent() {

    $node_component = [];

    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $nid = $node->id();
      $bundle = $node->bundle();

      $node_component = [
        'id' => $nid,
        'bundle' => $bundle
      ];
    }

    return $node_component;
  }

  /**
   * Build tail component for nodes keyed by vocabulary type.
   *
   * @return array
   */
  private function buildTaxonomyComponent() {

    $term_component = [];

    $term = \Drupal::routeMatch()->getParameter('taxonomy_term');
    if($term instanceof \Drupal\taxonomy\Entity\Term) {
      $nid = $term->id();
      $bundle = $term->bundle();

      $term_component = [
        'id' => $nid,
        'bundle' => $bundle
      ];
    }
    return $term_component;
  }

  /**
   * Build tail component for nodes keyed by 'view'.
   *
   * @return array
   */
  private function buildViewComponent() {

    $view_component = [];

    //@TODO Clean up - add validation.
    $route = \Drupal::routeMatch()->getRouteObject();
    $view_component = [
      'id'        => $route->getDefault('view_id'),
      'bundle'    => 'view',
      'args'      => 'view args', //@TODO ...
      'callback'  => 'views_get_view'
    ];


    return $view_component;
  }

}