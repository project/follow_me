<?php

namespace Drupal\follow_me;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Cache\Cache;
use Drupal\Component\Utility\UrlHelper;
use Drupal\views\Views;


/**
 * Class CurrentTrailService.
 */
class CurrentTrailService implements OutboundPathProcessorInterface {

  /**
   * @var
   */
  protected $currentTrailId;

  /**
   * Constructs a new CurrentTrailService object.
   */
  public function __construct($currentTrailId = null) {
    $this->currentTrailId = $currentTrailId;
  }

  /**
   * Gets the unique identifier for the trail.
   *
   * @return null|string
   */
  public function getCurrentTrailId() {
    $currentTrailId = null;
    /**
     * Check the request session for follow_me:id.
     */
    if (\Drupal::service('tempstore.private')->get('follow_me')->get('follow_me:id')) {
      $currentTrailId = \Drupal::service('tempstore.private')->get('follow_me')->get('follow_me:id');
    }
    return $currentTrailId;
  }

  /**
   * @return mixed
   */
  public function getTrail() {
    // Get Current trail.
    // @TODO: Clean up (create as method).
    $current_trail_id = $this->getCurrentTrailId();
    $cid = 'follow_me:'. $current_trail_id;

    // Load cached trail.
    $cache = \Drupal::cache()->get($cid);
    $trail = $cache->data;

    return $trail;
  }

  /**
   * @return mixed
   */
  public function getPrevious() {
    $trail = $this->getTrail();
    $previous = array_slice($trail, 1, 1);
    return $previous;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = array(), Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {

    // Ignore admin paths.
    // Check if the route is an admin link.
    // This appears to be faster than using:
    // if (\Drupal::service('router.admin_context')->isAdminRoute()) {
    //   return FALSE;
    // }
    $is_admin = \Drupal::service('router.admin_context')->isAdminRoute();

    if (!$is_admin) {
      $current_trail_id = $this->getCurrentTrailId();
      $current_trail_component = $this->getTrailComponent();
      $cid = 'follow_me:'. $current_trail_id;

      if ($cache = \Drupal::cache()->get($cid)) {
        // Build new trail.
        // Fire once so that we don't get duplicates.
        if ( !defined('REBUILD_TRAIL') ) {
          $current_trail = $this->buildTrail($current_trail_component);
          $pruned_trail = $this->pruneTrail($current_trail);
          \Drupal::cache()->set($cid, $pruned_trail);

          $route = \Drupal::routeMatch()->getRouteObject();

          // Might need to pass the view ID to get view details.
          $view_id =$route->getDefault('view_id');

          define('REBUILD_TRAIL', TRUE);
        }

      } else {
        // Build initial trail and save to cache.
        $current_trail = $this->buildTrail($current_trail_component);
        \Drupal::cache()->set($cid, $current_trail);
      }
    }

    return $path;

  }


  /**
   * @param array $current_trail_component
   *
   * @return array
   */
  private function buildTrail(array $current_trail_component) {

    $current_trail_id = $this->getCurrentTrailId();
    $current_trail = \Drupal::cache()->get('follow_me:'. $current_trail_id);

    // if trial is empty, return current trail
    if(empty($current_trail)) {
      $trail = $current_trail_component;
    } else {
      $saved_trail = $current_trail->data;
      $trail = array_merge($current_trail_component, $saved_trail);
    }

    return $trail;

  }

  /**
   * @param array $trail
   *
   * @return array
   */
  private function pruneTrail(array $trail) {
    // Check trail for any duplicates and remove them.
    $pruned_trail = array_unique($trail, SORT_REGULAR);
    return $pruned_trail;
  }



  /**
   * Creates a trail component to wrap the specified Follow Me context.
   *
   * @return array
   */
  private function getTrailComponent() {

    $current_route_name = \Drupal::service('current_route_match')->getRouteName();
    $component = [];
    if ($current_route_name == null) {
      return $component;
    }

    // build node component.
    if (str_starts_with($current_route_name, 'entity.node')) {
      $component = $this->buildNodeComponent();
    }
    // build term component.
    if (str_starts_with($current_route_name, 'entity.taxonomy_term')) {
      $component = $this->buildTaxonomyComponent();
    }
    // build view component.
    if (str_starts_with($current_route_name, 'view')) {
      $component = $this->buildViewComponent();
    }

    return $component;
  }

  /**
   * Build tail component for nodes keyed by content type.
   *
   * @return array
   */
  private function buildNodeComponent() {

    $node_component = [];

    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof \Drupal\node\NodeInterface) {
      $nid = $node->id();
      $bundle = $node->bundle();

      $node_component[] = [
        'id' => $nid,
        'bundle' => $bundle
      ];
    }

    return $node_component;
  }

  /**
   * Build tail component for nodes keyed by vocabulary type.
   *
   * @return array
   */
  private function buildTaxonomyComponent() {

    $term_component = [];

    $term = \Drupal::routeMatch()->getParameter('taxonomy_term');
    if($term instanceof \Drupal\taxonomy\Entity\Term) {
      $nid = $term->id();
      $bundle = $term->bundle();

      $term_component[] = [
        'id' => $nid,
        'bundle' => $bundle
      ];
    }
    return $term_component;
  }

  /**
   * Build tail component for nodes keyed by 'view'.
   *
   * @return array
   */
  private function buildViewComponent() {

    $view_component = [];

//    $view_id =$route->getDefault('view_id');
//    $view = Views::getView($view_id);
//    ksm($view);

    //@TODO Clean up - add validation.
    $route = \Drupal::routeMatch()->getRouteObject();
    $view_component[] = [
      'id'        => $route->getDefault('view_id'),
      'bundle'    => 'view',
      'args'      => 'view args', //@TODO ...
      'callback'  => 'views_get_view'
    ];
    return $view_component;
  }

}
