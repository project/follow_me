<?php

/**
 *
 */

namespace Drupal\follow_me\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Class FollowMeSubscriber.
 */
class FollowMeSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['getTrailID'];
    return $events;
  }

  /**
   * Sets extra HTTP header for follow_me:id.
   */
  public function getTrailID() {
    // Already have an ID.
    if (\Drupal::service('tempstore.private')->get('follow_me')->get('follow_me:id')) {
      return;
    }
    // Save ID to tempstore.
    $follow_me_id = \Drupal::service('tempstore.private')->get('follow_me');
    $follow_me_id->set('follow_me:id', $this->generateTrailID());

    // Add to session?
    $session = \Drupal::request()->getSession();
    $session->set('follow_me:id', $this->generateTrailID());

  }

  /**
   * Generates an unique ID based on user type.
   *
   * @return string
   */
  public function generateTrailID() {
    $user_id = &drupal_static(__FUNCTION__);
    if (\Drupal::currentUser()->isAnonymous()) {
      $random_string = random_bytes(10);
      $user_id = md5($random_string);
    } else {
      $user = \Drupal::currentUser();
      $random_string = $user->id() . random_bytes(10);
      $user_id = md5($random_string);
    }
    return $user_id;
  }

}